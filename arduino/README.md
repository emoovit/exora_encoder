# EXORA wheel encoder publisher
Author : Zulfaqar Azmi, Ahmad Muaz

Email  : zulfaqar@moovita.com

Department : eMooVit, MY

Last modified: 13-September-2018

# Dependencies
1. the `exora_abs_encoder.py` requires `moovita_message_definitions`.
2. the `exora_abs_encoder.py` requires `mooloc` module.

# How to run:
1. Paste`exora_abs_encoder.py` into `./localization/Buggy`.
2. Run $`python exora_abs_encoder.py`


# Last modified version comments:
Created ROS publisher

# Additional Link:
[Arduino IDE download link](https://www.arduino.cc/en/Main/Software) 
