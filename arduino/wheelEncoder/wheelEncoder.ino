// Read Exora Wheel/ABS sensor
// Author : Zulfaqar Azmi, Ahmad Muaz
// Email : zulfaqar@moovita.com
// Department : eMooVit, MY
// Last modified: 13-September-2018
// version comments - Read and publish the encoder's number of ticks

#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#define pi 3.1416

uint32_t rightTicks;
uint32_t leftTicks;
  
// Driver Green 3 | Shotgun Blue 2
Encoder rightWheel(3, 3);    // avoid using pins with LEDs attached, for single input encoder(no A and B), just add same number
Encoder leftWheel(2, 2);    // avoid using pins with LEDs attached, for single input encoder(no A and B), just add same number

void setup() 
{
  Serial.begin(115200);
}


void loop() 
{
  rightTicks  = rightWheel.read(); //
  leftTicks   = leftWheel.read(); //

  Serial.print(leftTicks);
  Serial.print(",");
  Serial.println(rightTicks);  

  Serial.flush();
}
  
