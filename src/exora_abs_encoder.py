#!/usr/bin/env python

""" EXORA wheel encoder
# Author : Zulfaqar Azmi, Ahmad Muaz
# Email  : zulfaqar@moovita.com, mzulfaqar88@gmail.com, amuaz@moovita.com
# Department : eMooVit, MY
# Last modified: 13-September-2018
# version comments - Created publisher for Exora Wheel Encoder
"""

import serial
import socket
import struct
import binascii
import time
import math
import numpy
import rospy
import string
from moovita_message_definitions.msg import encoder

WHEEL_RADIUS    = 0.35
DIST_PER_TICKS  = (math.pi * 2.0 * WHEEL_RADIUS)/196.0
prev_delta_left = -1.0
prev_delta_right = -1.0

try:
      ser = serial.Serial('/dev/ttyACM0', 115200)
except:
      ser = serial.Serial('/dev/ttyACM1', 115200)

def calculate_speed(left_ticks, right_ticks, differencess):
    v_left = float(delta_left) * ((math.pi * 2.0 * WHEEL_RADIUS)/196.0) / differencess
    #v_right = float(delta_right) * DIST_PER_TICKS / float((prev_time-cur_time).to_Sec())

    return v_left

def talker():
    rospy.init_node("exora_abs_encoder_")
    abs_pub = rospy.Publisher("/exora_encoder", encoder, queue_size=50)
    current_time    = rospy.Time.now()
    previous_time   = rospy.Time.now()
    msg = encoder()
    delta_left = 0.0
    delta_right = 0.0
    prev_delta_left = 0.0
    prev_delta_right = 0.0
    _arduino_serial_read = ""

    # r = rospy.Rate(10)
    while not rospy.is_shutdown():  # while forever ever loop
        
        while ser.inWaiting():
            try:
                _arduino_serial_read = ser.readline()
            except:
                pass
        # ser.flush()
        current_time = rospy.Time.now()
        print "_arduino_serial_read = ", _arduino_serial_read
        try:
            _data = _arduino_serial_read.split(',',1)
            msg.header.frame_id = 'exora'
            

            msg.tick_0 = int(_data[1],10)
            msg.tick_1 = int(_data[0],10)
            msg.tick_2 = int(_data[0],10)
            msg.tick_3 = int(_data[1],10)

            # delta is change of ticks in a specific time
            # change of ticks is current ticks - previous ticks

            #delta_left = (float(msg.tick_0 - prev_delta_left))  
            # delta_right = (float(msg.tick_1 - prev_delta_right))

            current_time = rospy.Time.now()

            msg.header.stamp = current_time
            # print "Msg", msg.tick_0 , msg.tick_1
            # print "Cur", delta_left , delta_right
            # print "prev", prev_delta_left , prev_delta_right

            # timeDiff = (float(current_time.secs - previous_time.secs) + (float(current_time.nsecs - previous_time.nsecs)/1000000000.0))
            # timeDiff = float(current_time.nsecs - previous_time.nsecs)
            
            # vehicle speed = (wheel speed left + wheel speed right) / 2
            # wheel speed left = delta left / time diff;
            # wheel speed right = delta right / time diff;
            wheel_speed_left = ((float(msg.tick_0 - prev_delta_left))*(math.pi * 2.0 * 0.35)/196.0) / (float(current_time.secs - previous_time.secs) + (float(current_time.nsecs - previous_time.nsecs)/1000000000.0))
            wheel_speed_right = ((float(msg.tick_1 - prev_delta_right))*(math.pi * 2.0 * 0.35)/196.0) / (float(current_time.secs - previous_time.secs) + (float(current_time.nsecs - previous_time.nsecs)/1000000000.0))
            vehicle_speed = (wheel_speed_left + wheel_speed_right) / 2

            msg.speed  = vehicle_speed

            wheel_speed_left = wheel_speed_left * 3.6
            wheel_speed_right = wheel_speed_right * 3.6
            vehicle_speed = vehicle_speed*3.6

                     
            prev_delta_left     = msg.tick_0
            prev_delta_right    = msg.tick_1

            previous_time = current_time
        
            # print (msg.speed * 3.6)
            print "speedLeft",round(wheel_speed_left,4)
            print "speedRight",round(wheel_speed_right,4)
            print "speed",round(vehicle_speed,4)

            # r.sleep()
            # rospy.loginfo(msg)
        
        except:
            pass
        abs_pub.publish(msg)
        
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

